"use strict";
const { sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async findSlug(ctx) {
    const entity = await strapi.services.posts.findSlug(ctx.params);
    return sanitizeEntity(entity, { model: strapi.models.posts });
  }
};

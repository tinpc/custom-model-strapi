"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
  findSlug(params) {
    let query;
    if (params._slug) query = params._slug;
    else query = params.slug;
    return strapi.query("posts").model.findOne({
      slug: query
    });
  }
};
